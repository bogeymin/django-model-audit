import os
from setuptools import setup

def read(file_path):
    """Shortcut for reading in the long description from a file that is
    relative to the setup script.."""
    return open(os.path.join(os.path.dirname(__file__), file_path)).read()

setup(
    name="django-model-audit",
    version="0.1.0d",
    author="F.S. Davis",
    author_email="consulting@fsdavis.com",
    description = ("Simple tools for  tracking added/modified user and times."),
    license="BSD",
    keywords="django database audit",
    url="http://bitbucket.org/bogeymin/django-audit",
    packages=["audit", ],
    long_description=read('readme.rst'),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        'Environment :: Web Environment',
        "Framework :: Django",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        'Operating System :: OS Independent',
        "Programming Language :: Python",
        "Topic :: Database",
    ],
)
