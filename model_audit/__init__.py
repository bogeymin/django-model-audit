# Django Imports
from django.conf import settings

# Define dependencies as a list of dictionary items. The dictionary should
# include a name and the module which it represents. The module should be
# importable, while the name should be human-friendly.
dependencies = (
    {'name': 'Django Model Utils', 'module': 'model_utils'},
)

# Dependency errors are collected into a list for late consumption.
errors = []

# Check that the required applications are installed.
for app in dependencies:
    try:
        __import__(app['module'])
    except ImportError:
        errors.append("The '%s' application is required, but cannot be imported." % app['name'])
    finally:
        if not app['module'] in settings.INSTALLED_APPS:
            errors.append("The '%s' application is required, but is not in INSTALLED_APPS." % app['name'])

