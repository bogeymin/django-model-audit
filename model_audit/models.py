"""
Model Audit provides stand-alone data models that are connected to Django's
content types. The optional mixins provide easy integration for access to audit
data directly from your model.
"""
# Python Imports

# Django Imports
from django.contrib.auth.models import User
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Local Imports

# Choices

# Models



class AAuditBase(models.Model):
    """Abstract model that defines the connection to contenttypes for use by
    other audit models.
    """

    content_object = generic.GenericForeignKey(
        ct_field='content_type',
        fk_field='object_id'
    )

    content_type = models.ForeignKey(
        ContentType,
        help_text=_("The Django content type."),
        verbose_name=_("content type")
    )

    object_id = models.IntegerField(
        _("object id"),
        help_text=_("ID of the participating object.")
    )

    class Meta:
        abstract = True


class TimeAudit(AAuditBase):
    """Provides ``added_date_time`` and ``modified_date_time``."""
    added_date_time = models.DateTimeField(
        _('created date and time'),
        help_text=_("Date and time the record was added.")
    )

    modified_date_time = models.DateTimeField(
        _('modified date and time'),
        help_text=_("Date and time the record was last updated.")
    )


class TimeAuditMixin(models.Model):
    """Provides access to ``added_date_time`` and ``modified_date_time``
    properties.
    """

    class Meta:
        abstract = True

    @property
    def added_date_time(self):
        return self.audit_time().added_date_time

    def audit_time(self):
        """Return a ``TimeAudit`` instance for the current object."""
        try:
            ThisContentType = ContentType.objects.get_for_model(self)
            return TimeAudit.objects.get(content_type__pk=ThisContentType.id, object_id=self.pk)
        except TimeAudit.DoesNotExist:
            return TimeAudit()

    @property
    def modified_date_time(self):
        return self.audit_time().modified_date_time


class UserAudit(AAuditBase):
    """Provides a ``added_by`` and ``modified_by`` for the current user."""

    added_by = models.ForeignKey(
        User,
        related_name="added_records",
        verbose_name = _("added by")
    )

    modified_by = models.ForeignKey(
        User,
        related_name="modified_records",
        verbose_name = _("modified by")
    )

    class Meta:
        verbose_name = _("User Audit Record")
        verbose_name_plural = _("User Audit Records")


class UserAuditMixin(models.Model):
    """Provides access to ``added_by`` and ``modified_by`` properties."""

    class Meta:
        abstract = True

    @property
    def added_by(self):
        return self.audit_user().added_by.get_full_name() or self.audit_user().added_by

    def audit_user(self):
        """Return a ``UserAudit`` instance for the current object."""
        try:
            ThisContentType = ContentType.objects.get_for_model(self)
            return UserAudit.objects.get(content_type__pk=ThisContentType.id, object_id=self.pk)
        except UserAudit.DoesNotExist:
            return UserAudit()

    @property
    def modified_by(self):
        return self.audit_user().modified_by.get_full_name() or self.audit_user().modified_by

