"""
Model Audit utilities. Currently, the only feature is to provide access to the
appropriate ``now()`` function based on timezone settings.
"""

# Python Imports
from datetime import datetime

# Django Imports
from django.conf import settings

if settings.USE_TZ:
    from django.utils.timezone import now
else:
    now = datetime.now
