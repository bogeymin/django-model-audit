# Django Imports
from django.db import models

# Local Imports
from utils import now

# Fields


class AutoAddDateTimeField(models.DateTimeField):
    """Standard ``DateTimeField`` which automatically sets itself to the
    current date and time when the record is first saved.
    """
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('editable', False)
        kwargs.setdefault('default', now)
        super(AutoCreatedField, self).__init__(*args, **kwargs)


class AutoModifiedDateTimeField(AutoAddDateTimeField):
    """Extends ``AutoAddDateTimeField`` to automatically set itself to the
    current date and time whenever the record is saved.
    """
    def pre_save(self, model_instance, add):
        value = now()
        setattr(model_instance, self.attname, value)
        return value

