"""
The Model Audit API provides a simple interface for adding audit data from your
views or model admin.
"""

# Django Imports
from django.contrib.contenttypes.models import ContentType

# Local Imports
from models import TimeAudit, UserAudit
from utils import now

# Members


def time_audit(ContentObject):
    """Connect ``added_date_time`` and ``modified_date_time`` information to
    a given object."""
    ThisContentType = ContentType.objects.get_for_model(ContentObject)
    try:
        Audit = TimeAudit.objects.get(content_type__pk=ThisContentType.id, object_id=ContentObject.pk)
        Audit.modified_date_time = now()
    except TimeAudit.DoesNotExist:
        Audit = TimeAudit(content_object=ContentObject)
        Audit.added_date_time = now()
        Audit.modified_date_time = now()

    Audit.save()
    return Audit


def user_audit(UserInstance, ContentObject):
    """Connect ``added_by`` and ``modified_by`` information to a given
    object.
    """
    ThisContentType = ContentType.objects.get_for_model(ContentObject)
    try:
        Audit = UserAudit.objects.get(content_type__pk=ThisContentType.id, object_id=ContentObject.pk)
        Audit.modified_by = UserInstance
    except UserAudit.DoesNotExist:
        Audit = UserAudit(content_object=ContentObject)
        Audit.added_by = UserInstance
        Audit.modified_by = UserInstance

    Audit.save()
    return Audit

